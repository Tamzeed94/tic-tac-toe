/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tictactoe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 *
 * @author tamzeed94
 */
public class GUIclass extends javax.swing.JFrame {

    /**
     * Creates new form GUIclass
     */
    public Board brd1=new Board();
    //public int[][]ar=new int[3][3];
	int alpha=Integer.MIN_VALUE;
	int beta=Integer.MAX_VALUE;
	int i=0;
	public ArrayList<Board>brds=new  ArrayList<Board>();
	public void MiniMax()
	{
                Random rand=new Random();
                int x=rand.nextInt((2 - 0) + 1) ;
                int y=rand.nextInt((2 - 0) + 1) ;
		brd1.board[x][y]=1;
                setGui(brd1.board);
			
	}
	public int maxFunc(Board brd)
	{
		//if(beta<=alpha)return Integer.MAX_VALUE;
		if(brd.isGoal()!=0)
		{
			//ar=brd.getBoard();
			return brd.isGoal();
		}
		if(brd.isFinished()==1)
		{
			//ar=brd.getBoard();
			return 0;
		}
		int v=-100;
		ArrayList<Board>actions=(ArrayList<Board>) brd.actionsM(1);
		for (Board board : actions) {
			
			int temp=minFunc(board);
			if(temp>v)
			{
				v=temp;
			}
			Board chld=new Board();
			chld.setBoard1(chld, board.board);
			chld.val=temp;
			brd.child.add(chld);
			if(v>=beta)
				return v;
			alpha=Math.max(v, alpha);
			//if(v>=beta)return v;
			
			
			
		}		
		return v;
	}
	public int minFunc(Board brd)
	{
		//if(beta<=alpha)return Integer.MIN_VALUE;
		if(brd.isGoal()!=0)
		{
			return brd.isGoal();
		}
		if(brd.isFinished()==1)
		{
			return 0;
		}
		int v=100;
		ArrayList<Board>actions=(ArrayList<Board>) brd.actionsM(2);
		for (Board board : actions) {
			
			int temp=maxFunc(board);
			if(temp<v)
			{
				v=temp;
			}
			
		}
		if(v<=alpha)
			return v;
		beta=Math.min(v, beta);
		
		return v;
	}

        public void setGui(int ar[][])
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    if(ar[i][j]!=0)
                    {
                        if(i==0 && j==0)
                    {
                        if(ar[i][j]==1)
                            jButton00.setText("X");
                        else if(ar[i][j]==2)
                            jButton00.setText("O");
                        jButton00.setEnabled(false);
                    }
                    else if(i==0 && j==1)
                    {
                        if(ar[i][j]==1)
                            
                            jButton01.setText("X");
                        else if(ar[i][j]==2)
                            jButton01.setText("O");
                        
                        jButton01.setEnabled(false);
                    }
                    else if(i==0 && j==2)
                    {
                        if(ar[i][j]==1)
                            jButton02.setText("X");
                        else if(ar[i][j]==2)
                            jButton02.setText("O");
                        jButton02.setEnabled(false);
                    }
                    else if(i==1 && j==0)
                    {
                        if(ar[i][j]==1)
                            jButton10.setText("X");
                        else if(ar[i][j]==2)
                            jButton10.setText("O");
                        jButton10.setEnabled(false);
                    }
                    else if(i==1 && j==1)
                    {
                        if(ar[i][j]==1)
                            jButton11.setText("X");
                        else if(ar[i][j]==2)
                            jButton11.setText("O");
                        jButton11.setEnabled(false);
                    }
                    else if(i==1 && j==2)
                    {
                        if(ar[i][j]==1)
                            jButton12.setText("X");
                        else if(ar[i][j]==2)
                            jButton12.setText("O");
                        jButton12.setEnabled(false);
                    }
                    else if(i==2 && j==0)
                    {
                        if(ar[i][j]==1)
                            jButton20.setText("X");
                        else if(ar[i][j]==2)
                            jButton20.setText("O");
                        jButton20.setEnabled(false);
                    }
                    else if(i==2 && j==1)
                    {
                        if(ar[i][j]==1)
                            jButton21.setText("X");
                        else if(ar[i][j]==2)
                            jButton21.setText("O");
                        jButton21.setEnabled(false);
                    }
                    else if(i==2 && j==2)
                    {
                        if(ar[i][j]==1)
                            jButton22.setText("X");
                        else if(ar[i][j]==2)
                            jButton22.setText("O");
                        jButton22.setEnabled(false);
                    }
                    
                    }
                    
                    
                    
                    
                }
                
            }
        }
	
        public Board checker(Board brd)
	{
		System.out.println("checking");
		Board temp=new Board();
		int val=-100;
		
		for (Board ob : brd.child) {
			int tmp=ob.val;
			System.out.println("val: "+tmp);
			if(tmp>val)
			{
				val=tmp;
				temp.setBoard1(temp, ob.board);
				
				
				
			}
		}
		return temp;
	}
        public int minUser(Board brd)
	{
		while(true)
		{
			if(brd.isGoal()!=0)
			{
				return brd.isGoal();
			}
			if(brd.isFinished()==1)
			{
				return 0;
			}
			System.out.println("input: ");
			Scanner scan=new Scanner(System.in);
			int x=scan.nextInt();
			int y=scan.nextInt();
			brd.board[x][y]=2;
			System.out.println("after min");
			brd.printBoard();
			//brd.depth=0;
			Board maxBoard=new Board();
			maxBoard.setBoard1(maxBoard, brd.board);
			int val=maxFunc(maxBoard);
			Board brdTmp=checker(maxBoard);
			
			brd.setBoard1(brd, brdTmp.board);
			System.out.println("after max");
			brd.printBoard();
			
			
			
		}
		
	}
        
        public int minUser(int x,int y)
        {
            
            int n;
			System.out.println("input: ");
			
			brd1.board[x][y]=2;
			System.out.println("after min");
			brd1.printBoard();
                        setGui(brd1.board);
			//brd.depth=0;
			Board maxBoard=new Board();
			maxBoard.setBoard1(maxBoard, brd1.board);
			
			int val=maxFunc(maxBoard);
			Board brdTmp=checker(maxBoard);
			
			brd1.setBoard1(brd1, brdTmp.board);
			System.out.println("after max");
			brd1.printBoard();
                        setGui(brd1.board);
                        if(brd1.isGoal()!=0)
			{
                            if(brd1.isGoal()==1)
                            
                                  //JOptionPane.showMessageDialog(null, "LOST THE GAME");
                                 n=JOptionPane.showConfirmDialog(null,"restart?","lost the game",JOptionPane.YES_NO_OPTION);
                            else
                                 n=JOptionPane.showConfirmDialog(null,"restart?","won the game",JOptionPane.YES_NO_OPTION);
                            System.out.println("NNN :"+n);
                            if(n==0)
                            {
                                this.setVisible(false);
                                Board tmp=new Board();
                                for(int i=0;i<3;i++)
                                {
                                        for(int j=0;j<3;j++)
                                        {
                                                if(i==1 &j!=2)
                                                        tmp.board[i][j]=1;
                                                if(i==0)
                                                tmp.board[i][j]=0;


                                        }
                                }
                                       GUIclass gc=new GUIclass();
                                       gc.MiniMax();
                                       gc.setVisible(true);
                            }
                                    else
                                    {
                                    this.setVisible(false);
                                }
                            return brd1.isGoal();
			}
			if(brd1.isFinished()==1)
			{
                            n=JOptionPane.showConfirmDialog(null,"restart?","drew the game",JOptionPane.YES_NO_OPTION);
                            System.out.println("NNN :"+n);
                            if(n==0)
                            {
                                this.setVisible(false);
                                Board tmp=new Board();
                                for(int i=0;i<3;i++)
                                {
                                        for(int j=0;j<3;j++)
                                        {
                                                if(i==1 &j!=2)
                                                        tmp.board[i][j]=1;
                                                if(i==0)
                                                tmp.board[i][j]=0;


                                        }
                                }
                                       GUIclass gc=new GUIclass();
                                       gc.MiniMax();
                                       gc.setVisible(true);
                            }
                                    else
                                    {
                                    this.setVisible(false);
                                }
                                return 0;
                            }
			
			
        return 0;
        }
//        public int minUser(int x,int y)
//	{
//		//while(true)
//		{
//			System.out.println("input: ");
//			//Scanner scan=new Scanner(System.in);
//			brd1.board[x][y]=2;
//			System.out.println("after min");
//			brd1.printBoard();
//                        brd1.level=0;
//                        
//			int val=maxFunc(brd1);
//                        Board tmps=checker(brd1);
//                       
//			brd1.setBoard1(brd1, tmps.board);
//                        
//			System.out.println("after max");
//			brd1.printBoard();
//			System.out.println("arr");
//			brd1.printBoard(ar);
//                        setGui(brd1.board);
//                        int n=-1;
//			if(brd1.isGoal()!=0)
//			{
//                            if(brd1.isGoal()==1)
//                            
//                                  //JOptionPane.showMessageDialog(null, "LOST THE GAME");
//                                 n=JOptionPane.showConfirmDialog(null,"restart?","lost the game",JOptionPane.YES_NO_OPTION);
//                            else
//                                 n=JOptionPane.showConfirmDialog(null,"restart?","won the game",JOptionPane.YES_NO_OPTION);
//                            System.out.println("NNN :"+n);
//                            if(n==0)
//                            {
//                                this.setVisible(false);
//                                Board tmp=new Board();
//                                for(int i=0;i<3;i++)
//                                {
//                                        for(int j=0;j<3;j++)
//                                        {
//                                                if(i==1 &j!=2)
//                                                        tmp.board[i][j]=1;
//                                                if(i==0)
//                                                tmp.board[i][j]=0;
//
//
//                                        }
//                                }
//                                       GUIclass gc=new GUIclass();
//                                       gc.MiniMax(tmp);
//                                       gc.setVisible(true);
//                            }
//                                    else
//                                    {
//                                    this.setVisible(false);
//                                }
//                            return brd1.isGoal();
//			}
//			if(brd1.isFinished()==1)
//			{
//                            n=JOptionPane.showConfirmDialog(null,"restart?","drew the game",JOptionPane.YES_NO_OPTION);
//                            System.out.println("NNN :"+n);
//                            if(n==0)
//                            {
//                                this.setVisible(false);
//                                Board tmp=new Board();
//                                for(int i=0;i<3;i++)
//                                {
//                                        for(int j=0;j<3;j++)
//                                        {
//                                                if(i==1 &j!=2)
//                                                        tmp.board[i][j]=1;
//                                                if(i==0)
//                                                tmp.board[i][j]=0;
//
//
//                                        }
//                                }
//                                       GUIclass gc=new GUIclass();
//                                       gc.MiniMax(tmp);
//                                       gc.setVisible(true);
//                            }
//                                    else
//                                    {
//                                    this.setVisible(false);
//                                }
//                                return 0;
//                            }
//			
//			
//		}
//                return 0;
//		
//	}
    public GUIclass() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton00 = new javax.swing.JButton();
        jButton02 = new javax.swing.JButton();
        jButton01 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton00ActionPerformed(evt);
            }
        });

        jButton02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton02ActionPerformed(evt);
            }
        });

        jButton01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton01ActionPerformed(evt);
            }
        });

        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jButton00, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton02, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 9, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(146, Short.MAX_VALUE)
                    .addComponent(jButton01, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(142, 142, 142)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton02, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton00, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addComponent(jButton01, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(295, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton00ActionPerformed
        // TODO add your handling code here:
        jButton00.setText("O");
        jButton00.setEnabled(false);
        minUser( 0, 0);
        //jButton1.set
    }//GEN-LAST:event_jButton00ActionPerformed

    private void jButton01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton01ActionPerformed
        // TODO add your handling code here:
        jButton01.setText("O");
        jButton01.setEnabled(false);
        minUser( 0, 1);
      //  jButton3.setOnClickListener(null);
    }//GEN-LAST:event_jButton01ActionPerformed

    private void jButton02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton02ActionPerformed
        // TODO add your handling code here:
        jButton02.setText("O");
        jButton02.setEnabled(false);
        minUser( 0, 2);
       // jButton2.setOnClickListener(null);
    }//GEN-LAST:event_jButton02ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        jButton10.setText("O");
        jButton10.setEnabled(false);
        minUser( 1, 0);
      //  jButton5.setOnClickListener(null);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        jButton11.setText("O");
        jButton11.setEnabled(false);
        minUser( 1, 1);
       // jButton4.setOnClickListener(null);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        jButton12.setText("O");
        jButton12.setEnabled(false);
        minUser( 1, 2);
       // jButton6.setOnClickListener(null);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
        jButton20.setText("O");
        jButton20.setEnabled(false);
        minUser( 2, 0);
        //jButton8.setOnClickListener(null);
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        // TODO add your handling code here:
        jButton21.setText("O");
        jButton21.setEnabled(false);
        minUser( 2, 1);
        //jButton9.setOnClickListener(null);
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
        jButton22.setText("O");
        jButton22.setEnabled(false);
        minUser( 2, 2);
    }//GEN-LAST:event_jButton22ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUIclass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUIclass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUIclass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUIclass.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Board tmp=new Board();
                for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				tmp.board[i][j]=0;
				
				
			}
		}
               GUIclass gc=new GUIclass();
               gc.MiniMax();
               gc.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton00;
    private javax.swing.JButton jButton01;
    private javax.swing.JButton jButton02;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    // End of variables declaration//GEN-END:variables
}
