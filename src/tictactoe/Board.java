package tictactoe;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Board {
	public int board[][]=new int[3][3];
	//public Board parent=new Board();
	public int pboard[][]=new int[3][3];
	public int val;
	//public int depth;
	public ArrayList<Board> child=new ArrayList<Board>();

	public int[][] getBoard() {
		return board;
	}

	public void setBoard1(Board brd,int[][] board1) {
		brd.board = board1;
	}
	public void setTile(int x,int y,int target)
	{
		board[x-1][y-1]=target;
		
	}
	public void printBoard()
	{
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(this.board[i][j]+" ");
			}
			System.out.println();
		}
	}
	public void printBoard(int brd[][])
	{
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(brd[i][j]+" ");
			}
			System.out.println();
		}
	}
	public int checkRows()
	{
		int count1=0,count2=0;
		//check rows
		for (int i=0;i<3;i++)
		{
			count1 =0;count2=0;
			for(int j=0;j<3;j++)
			{
				if(board[i][j]==1)
				{
					count1++;
				}
				if(board[i][j]==2)
				{
					count2++;
				}
			}
			if(count1==3)
			{
				return 1;
			}
			else if(count2==3)
			{
				return -1;
			}
		}
		return 0;
	}
	public int checkColumns()
	{
		int count1=0,count2=0;
		//check rows
		for (int i=0;i<3;i++)
		{
			count1 =0;count2=0;
			for(int j=0;j<3;j++)
			{
				if(board[j][i]==1)
				{
					count1++;
				}
				if(board[j][i]==2)
				{
					count2++;
				}
			}
			if(count1==3)
			{
				return 1;
			}
			else if(count2==3)
			{
				return -1;
			}
		}
		return 0;
		
	}
	public int checkDiagonal()
	{
		int count1=0,count2=0;
		for(int i=0;i<3;i++)
		{
			if(board[i][i]==1)
				count1++;
			if(board[i][i]==2)
				count2++;
		}
		if(count1==3)
			return 1;
		if(count2==3)
			return -1;
		count1=0;
		count2=0;
		if(board[0][2]==1)
                {
                    count1++;
                }
                if(board[0][2]==2)
                {
                    count2++;
                }
               if(board[1][1]==1)
                {
                    count1++;
                }
                if(board[1][1]==2)
                {
                    count2++;
                }
                if(board[2][0]==1)
                {
                    count1++;
                }
                if(board[2][0]==2)
                {
                    count2++;
                }
		if(count1==3)
			return 1;
		if(count2==3)
			return -1;
		
		
		return 0;
	}
	public int isGoal()
	{
		int res1=this.checkColumns();
		if(res1!=0)return res1;
		int res2=this.checkRows();
		if(res2!=0)return res2;
		int res3=this.checkDiagonal();
		if(res3!=0)return res3;
		return 0;
	}
	public int isFinished()
	{
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				if(this.board[i][j]==0)
					return 0;
			}
			
		}
		return 1;
	}
	public int utility()
	{
		int ret=isGoal();
		if(ret==1)return 1;
		if(ret==2)return -1;
		return 0;
	}
	public void copyBoard(int [][]board)
	{
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j]=this.board[i][j];
		}
		
	}
	public Iterable<Board> actionsM(int type)
	{
		ArrayList<Board> actions=new ArrayList<Board>();
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				if(this.board[i][j]==0)
				{
					
					int [][]temp=new int[3][3];
					this.copyBoard(temp);

					if(type==1)
						temp[i][j]=1;
					else
						temp[i][j]=2;
					Board tempBrd=new Board();
					tempBrd.setBoard1(tempBrd,temp);
					this.copyBoard(tempBrd.pboard);
					actions.add(tempBrd);
					//brds.add(tempBrd);
				}
			}
		}
		return actions;
		
	}
	

	public static void main(String args[]) throws Exception, Exception
	{
		Board brd=new Board();
		int [][]blocks=new int[3][3];
		try (BufferedReader br = new BufferedReader(new FileReader("brd.txt"))) {
		    String line;
		    String nums[]=new String[3*2];
		    int i=0;
		    int tmpInd=0;
		    while ((line = br.readLine()) != null) {
		    	if(tmpInd++>0)
		    	{
		    		nums=line.split(" ");
			    	//System.out.println("sz: "+nums.length);
			    	for(int j=0;j<3;j++)
			    	{
			    		blocks[i][j]=Integer.parseInt(nums[j]) ;
			    		System.out.println("blocks:"+ blocks[i][j]);
			    	}
			    	i++;
			     	
		    	}
		    
		    }
		}
		
		brd.setBoard1(brd,blocks);
		System.out.println(brd.isGoal());
		System.out.println();
		ArrayList<Board> brds=(ArrayList<Board>) brd.actionsM(1);
		for (Board tmp : brds) {
			
			tmp.printBoard();
			System.out.println();
		}
		brd.printBoard();
		
		
	}

}
